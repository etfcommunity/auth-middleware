var jwt = require('jsonwebtoken')
, _ = require('lodash');

module.exports.jwt = (secret) =>
    function (req, res, next) {
        var token = '';
        if (req.headers.authorization) {
            token = req.headers.authorization.replace(/^Bearer/i, '').trim();
            return jwt.verify(token, secret, (err, decoded) => {
                if (err) return res.status(403).send();
                req.jwt = decoded;
                return next();
            });
        }
        return res.status(403).send();
    };


module.exports.allow = function allow() {
    var roles = arguments;
    return (req, res, next) => {
        var userRoles = req.jwt.roles;
        if (!_.intersection(userRoles, roles).length) {
            return res.status(403).send();
        }
        return next();
    };
};
